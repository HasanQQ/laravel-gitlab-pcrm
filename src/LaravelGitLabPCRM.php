<?php

namespace HasanQQ\LaravelGitLabPCRM;

use Illuminate\Support\Facades\File;

class LaravelGitLabPCRM
{
    public static ?string $accessToken = null;
    public static ?array  $namespaces  = null;

    public static function getAccessToken()
    {
        if (self::$accessToken !== null)
            return self::$accessToken;

        $authJson = base_path('auth.json');

        if (!File::exists($authJson))
            throw new \RuntimeException('LaravelGitLabPCRM needs credentials from auth.json');

        $authJson = File::get($authJson);
        $authJson = json_decode($authJson, true);

        if (!is_array($authJson) || !isset($authJson['gitlab-token']['gitlab.com']))
            throw new \RuntimeException('LaravelGitLabPCRM needs credentials from auth.json');

        self::$accessToken = $authJson['gitlab-token']['gitlab.com'];

        return self::$accessToken;
    }

    public static function getNamespaces()
    {
        if (self::$namespaces !== null)
            return self::$namespaces;

        $composerJson = base_path('composer.json');

        if (!File::exists($composerJson))
            throw new \RuntimeException('LaravelGitLabPCRM needs composer.json on your base directory');

        $composerJson = File::get($composerJson);
        $composerJson = json_decode($composerJson, true);

        if (!is_array($composerJson) || !isset($composerJson['repositories']))
            throw new \RuntimeException('LaravelGitLabPCRM needs "repositories" index on your composer.json');

        $namespaces = [];

        foreach ($composerJson['repositories'] as $name => $repository) {
            if (preg_match('/^gitlab/', $name)) {
                $namespace = str_replace('gitlab/', '', $name);
                $url = $repository['url'] ?? null;

                if ($url === null)
                    throw new \RuntimeException('Missing url for ' . $name . ' repository on composer.json');

                preg_match('/\/group\/([0-9]+)\//', $url, $matches);

                $groupId = $matches[1] ?? null;

                if ($groupId === null)
                    throw new \RuntimeException('Missing group id for ' . $name . ' repository on composer.json');

                $namespaces[$namespace] = [
                    'id' => (int) $groupId,
                    'url' => $url,
                ];
            }
        }

        if (count($namespaces) === 0)
            throw new \RuntimeException('LaravelGitLabPCRM needs valid "repositories" on your composer.json');

        self::$namespaces = $namespaces;

        return self::$namespaces;
    }
}
