<?php

namespace HasanQQ\LaravelGitLabPCRM;

use HasanQQ\LaravelGitLabPCRM\Console\Commands\BuildGitModulesCommand;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider as SupportServiceProvider;

class ServiceProvider extends SupportServiceProvider
{
    public function boot()
    {
        Route::name('laravel_gitlab_pcrm.')
            ->prefix(config('laravel_gitlab_pcrm.path', 'gitlab-pcrm'))
            ->middleware(config('laravel_gitlab_pcrm.middleware', 'web'))
            ->group(function () {
                $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
            });

        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'laravel_gitlab_pcrm');

        if ($this->app->runningInConsole())
            $this->commands([
                BuildGitModulesCommand::class,
            ]);
    }
}
