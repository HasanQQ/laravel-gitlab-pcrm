<?php

namespace HasanQQ\LaravelGitLabPCRM\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class BuildGitModulesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'gitlab-pcrm:gitmodules';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Rebuild the .gitmodules file based on the packages directory';

    /**
     * Indicates whether the command should be shown in the Artisan command list.
     *
     * @var bool
     */
    protected $hidden = true;

    public function handle()
    {
        $packagesDir = base_path('/packages');
        $packageList = [];

        if (!File::exists($packagesDir) || !File::isDirectory($packagesDir))
            return;

        $directories = File::directories($packagesDir);

        foreach ($directories as $directory) {
            $dirName = explode('/', $directory);
            $dirName = Arr::last($dirName);

            $dirPath = $packagesDir . '/' . $dirName;
            $composerJson = $dirPath . '/composer.json';

            if (!File::isDirectory($packagesDir) || !File::exists($composerJson) || !File::isFile($composerJson))
                continue;

            $composerJson = json_decode(File::get($composerJson), true);

            if (!is_array($composerJson) || !isset($composerJson['name']))
                continue;

            $packageList[$composerJson['name']] = [
                'path' => './packages/' . $dirName,
                'url'  => 'git@gitlab.com:dummy-project/packages/' . Str::uuid() . '.git',
            ];
        }

        $context = '';

        foreach ($packageList as $name => $details) {
            $context .= '[submodule "' . $name . '"]' . "\n"
                . "\t" . 'path = ' . $details['path'] . "\n"
                . "\t" . 'url = ' . $details['url'] . "\n";
        }

        File::put(base_path('/.gitmodules'), $context);
    }
}
