<?php

namespace HasanQQ\LaravelGitLabPCRM\Http\Controllers\GitLab;

use HasanQQ\LaravelGitLabPCRM\Http\Controllers\Controller;
use HasanQQ\LaravelGitLabPCRM\LaravelGitLabPCRM;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Http;

class ProjectDetailsController extends Controller
{
    public function branches(Request $request, int $projectId)
    {
        $branches = Http::withHeaders([
            'PRIVATE-TOKEN' => LaravelGitLabPCRM::getAccessToken()
        ])
            ->get('https://gitlab.com/api/v4/projects/' . $projectId . '/repository/branches')
            ->json();

        $branches = Arr::sort($branches, function ($value) {
            return $value['commit']['authored_date'];
        });

        $branches = array_reverse($branches);

        return $branches;
    }

    public function packages(Request $request, string $namespace, string $repository)
    {
        // exist or 404
        $group = LaravelGitLabPCRM::getNamespaces()[$namespace] ?? abort(404);

        $packages = Http::withHeaders([
            'PRIVATE-TOKEN' => LaravelGitLabPCRM::getAccessToken()
        ])
            ->get(
                'https://gitlab.com/api/v4/group/' . $group['id']
                    . '/-/packages/composer/p2/' . $namespace . '/' . $repository . '.json'
            )
            ->json();

        $packages = $packages['packages'] ?? [];
        $packages = $packages[$namespace . '/' . $repository] ?? [];

        $response = [];

        foreach ($packages as $name => $package) {
            $response[$name] = [
                'dist' => $package['dist'],
                'source' => $package['source'],
                'uid' => $package['uid']
            ];
        }

        return $response;
    }

    public function publish(Request $request, int $projectId)
    {
        return Http::post(
            'https://__token__:' . LaravelGitLabPCRM::getAccessToken()
                . '@gitlab.com/api/v4/projects/'
                . $projectId . '/packages/composer',
            [
                'branch' => $request->input('branch')
            ]
        )
            ->json();
    }
}
