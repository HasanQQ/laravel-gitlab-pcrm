<?php

namespace HasanQQ\LaravelGitLabPCRM\Http\Controllers\GitLab;

use HasanQQ\LaravelGitLabPCRM\Http\Controllers\Controller;
use HasanQQ\LaravelGitLabPCRM\LaravelGitLabPCRM;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class GroupDetailsController extends Controller
{
    public function __invoke(Request $request, string $namespace)
    {
        // exist or 404
        $group = LaravelGitLabPCRM::getNamespaces()[$namespace] ?? abort(404);

        return view('laravel_gitlab_pcrm::home', [
            'group' => [
                'id' => $group['id'],
                'namespace' => $namespace
            ]
        ]);
    }

    public function details(Request $request, string $namespace)
    {
        // exist or 404
        $group = LaravelGitLabPCRM::getNamespaces()[$namespace] ?? abort(404);

        $details = Http::withHeaders([
            'PRIVATE-TOKEN' => LaravelGitLabPCRM::getAccessToken()
        ])
            ->get('https://gitlab.com/api/v4/groups/' . $group['id'])
            ->json();

        $response = [
            'id'        => $details['id'],
            'url'       => $details['web_url'],
            'name'      => $details['name'],
            'full_name' => $details['full_name'],
            'projects'  => []
        ];

        foreach ($details['projects'] as $project) {
            if (empty($project['description']))
                $project['description'] = null;

            $response['projects'][] = [
                'id' => $project['id'],
                'name' => $project['name'],
                'package_name' => 'shipsgo-website/' . $project['name'],
                'description' => $project['description'],
                'url' => $project['web_url'],
                'ssh_url_to_repo' => $project['ssh_url_to_repo'],
                'http_url_to_repo' => $project['http_url_to_repo'],
                'last_activity_at' => $project['last_activity_at'],
            ];
        }

        return $response;
    }
}
