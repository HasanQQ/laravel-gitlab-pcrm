<?php

namespace HasanQQ\LaravelGitLabPCRM\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;

class AssetController extends BaseController
{
    public function javascript()
    {
        $file = __DIR__ . '/../../../resources/js/application.js';
        $content = file_get_contents($file);

        return $this->cachedResponse($content, 'text/javascript');
    }

    public function css()
    {
        $file = __DIR__ . '/../../../resources/css/application.css';
        $content = file_get_contents($file);

        return $this->cachedResponse($content, 'text/css');
    }

    private function cachedResponse($content, $contentType)
    {
        $response = response($content, 200, [
            'Content-Type' => $contentType,
        ]);

        $response->setSharedMaxAge(31536000);
        $response->setMaxAge(31536000);
        $response->setExpires(new \DateTime('+1 year'));

        return $response;
    }
}
