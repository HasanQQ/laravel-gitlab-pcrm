((function (GitLabPCRM, $) {
    "use strict";

    if (typeof window.axios !== 'function')
        return console.error('"axios" not imported.');

    if (typeof window.moment !== 'function')
        return console.error('"moment" not imported.');

    if (typeof window.Swal !== 'function')
        return console.error('"sweetalert" not imported.');

    /////////////////////////////////////////////////////////////

    var App = function () {
        this.group = GitLabPCRM.group;
        this.projects = [];
    };

    /////////////////////////////////////////////////////////////

    App.prototype.getGroupDetails = function () {
        var $this = this;

        return axios
            .get('/' + GitLabPCRM.path + '/' + $this.group.namespace + '/details')
            .then(function (response) {
                let group = response.data;

                ////////////////////////////////////////////////// 
                // base details about group

                $('#group-avatar').text(group.name.slice(0, 1).toUpperCase());
                $('#group-name').html(
                    $('<a/>')
                        .text(group.full_name)
                        .attr('href', group.url)
                );
                $('#group-id').text(group.id);

                //////////////////////////////////////////////////

                $this.projects = group.projects;

                ////////////////////////////////////////////////// 
                // draw projects

                $('#packages').html('');

                group.projects.forEach(project => {
                    let div = $('<div/>')
                        .addClass('pb-3 mb-3 border-bottom');

                    ////////////////////////////////////////////////// 
                    // base details about project

                    $('<div/>')
                        .addClass('row mb-3')
                        .appendTo(div)
                        .append(
                            $('<div/>')
                                .addClass('col-sm-auto mb-2 mb-sm-0')
                                .html(
                                    $('<div/>')
                                        .addClass('ratio ratio-1x1 bg-danger rounded text-white fs-4')
                                        .css('width', '60px')
                                        .html(
                                            $('<div/>')
                                                .addClass('d-flex justify-content-center align-items-center')
                                                .text(project.name.slice(0, 1).toUpperCase())
                                        )
                                )
                        )
                        .append(
                            $('<div/>')
                                .addClass('col pt-1')
                                .append(
                                    $('<h5/>')
                                        .addClass('h6 text-truncate mb-0')
                                        .html(
                                            $('<a/>')
                                                .text(project.name)
                                                .attr('href', project.url)
                                        )
                                )
                                .append(
                                    $('<small/>')
                                        .addClass('text-muted')
                                        .text(project.description ?? '-')
                                )
                        )
                        .append(
                            $('<div/>')
                                .addClass('col-sm-auto mb-2 mb-sm-0')
                                .html(
                                    $('<small/>').addClass('text-muted pt-1')
                                        .text(moment(project.last_activity_at).fromNow())
                                )
                        );

                    ////////////////////////////////////////////////// 
                    // draw loading for branches

                    $('<div/>')
                        .addClass('row')
                        .appendTo(div)
                        .html(
                            $('<div/>')
                                .addClass('col-12 project-branches')
                                .attr('data-project-id', project.id)
                                .html(
                                    $('<small/>')
                                        .addClass('text-muted')
                                        .text('Branches Loading ...')
                                )
                        );

                    //////////////////////////////////////////////////

                    div.appendTo('#packages');
                });
            }).catch(function (error) {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'An error occurred while accessing the details of the group.',
                })
            });
    }

    /////////////////////////////////////////////////////////////

    App.prototype.getBranchesOfProject = function (project) {
        // find div
        let branchesDiv = $('.project-branches[data-project-id="' + project.id + '"]')

        return axios
            .get('/' + GitLabPCRM.path + '/projects/' + project.id + '/branches')
            .then(function (response) {
                let branches = response.data;

                project.branches = branches;

                if (branches.length === 0)
                    return branchesDiv.html(
                        $('<div/>')
                            .addClass('alert alert-secondary mb-0 small')
                            .append($('<i/>').addClass('fa fa-circle-info me-2'))
                            .append($('<span/>').text('There is no branch for this project.'))
                    );


                branchesDiv.html('');

                let listOfBranches = $('<ul/>')
                    .addClass('list-group')
                    .appendTo(branchesDiv);

                branches.forEach(branch => {
                    $('<li/>')
                        .addClass('list-group-item')
                        .appendTo(listOfBranches)
                        .html(
                            $('<div/>')
                                .addClass('row')
                                .append(
                                    $('<div/>')
                                        .addClass('col')
                                        .append(
                                            (() => {
                                                let elem = $('<div/>')
                                                    .addClass('mb-1')
                                                    .append($('<i/>').addClass('fa fa-fw fa-code-branch text-secondary me-3'))
                                                    .append($('<strong/>').addClass('pe-1').text(branch.name))

                                                if (branch.default)
                                                    elem.append($('<span/>').addClass('badge bg-primary rounded-pill ms-1').text('default'));

                                                if (branch.protected)
                                                    elem.append($('<span/>').addClass('badge bg-success rounded-pill ms-1').text('protected'));

                                                if (branch.merged)
                                                    elem.append($('<span/>').addClass('badge bg-info rounded-pill ms-1').text('merged'));

                                                return elem;
                                            })
                                        )
                                        .append(
                                            $('<div/>')
                                                .append($('<i/>').addClass('fa fa-fw fa-code-commit text-secondary me-3'))
                                                .append(
                                                    $('<a/>')
                                                        .attr('href', branch.commit.web_url)
                                                        .text(branch.commit.short_id)
                                                )
                                                .append($('<span/>').addClass('px-1').text(' · '))
                                                .append($('<span/>').text(branch.commit.message))
                                                .append($('<span/>').addClass('px-1').text(' · '))
                                                .append(
                                                    $('<small/>')
                                                        .addClass('text-muted')
                                                        .text(
                                                            moment(branch.commit.authored_date).fromNow()
                                                            + ' by ' + branch.commit.committer_name
                                                        )
                                                )
                                        )
                                )
                                .append(
                                    $('<div/>')
                                        .addClass('col-auto align-self-center branch-status')
                                        .attr('data-branch-name', branch.name)
                                        .html(
                                            $('<button/>')
                                                .addClass('btn btn-sm btn-secondary px-3')
                                                .prop('disabled', true)
                                                .html('Checking ...')
                                        )
                                )
                        );
                });

                //
            }).catch(function (error) {
                return branchesDiv.html(
                    $('<div/>')
                        .addClass('alert alert-danger mb-0 small')
                        .append($('<i/>').addClass('fa fa-circle-exclamation me-2'))
                        .append($('<span/>').text('An error occurred while accessing the branches of the project.'))
                );
            });
    }

    /////////////////////////////////////////////////////////////

    App.prototype.getCheckPackagesOfProject = function (project, branch) {
        var $this = this;
        var projectDiv = $('.project-branches[data-project-id="' + project.id + '"]');

        return axios
            .get('/' + GitLabPCRM.path + '/projects/' + $this.group.namespace + '/' + project.name + '/packages')
            .then(function (response) {
                let packages = response.data;

                for (const branch of project.branches) {
                    let packageStatusButtonDiv = projectDiv.find('.branch-status[data-branch-name="' + branch.name + '"]');
                    let statusOfBranch = null;

                    let button = $('<button/>').addClass('btn btn-sm px-3');

                    // missing
                    if (typeof packages['dev-' + branch.name] === 'undefined') {
                        statusOfBranch = 'PUBLISH';

                        button
                            .addClass('btn-warning')
                            .text('PUBLISH');

                        // not match
                    } else if (branch.commit.id !== packages['dev-' + branch.name].source.reference) {
                        statusOfBranch = 'UPDATE';

                        button
                            .addClass('btn-primary')
                            .text('UPDATE');
                    } else {
                        statusOfBranch = 'UP-TO-DATE';

                        button
                            .addClass('btn-success')
                            .text('UP-TO-DATE')
                            .prop('disabled', true);
                    }

                    if (statusOfBranch == 'PUBLISH' || statusOfBranch == 'UPDATE') {
                        button.on('click', function () {
                            let button = $(this).prop('disabled', true);

                            return axios.post('/' + GitLabPCRM.path + '/projects/' + project.id + '/publish', {
                                'branch': branch.name
                            }).then(() => {
                                button
                                    .removeClass('btn-warning')
                                    .removeClass('btn-primary')
                                    .addClass('btn-success')
                                    .prop('disabled', true)
                                    .text('UP-TO-DATE')
                                    .unbind('click');

                            }).catch(function (error) {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Oops...',
                                    text: 'An error occurred while publishing the branch of the project.',
                                })

                                button.prop('disabled', false);
                            });
                        })
                    }

                    packageStatusButtonDiv.html(button)
                }

                //
            }).catch(function (error) {
                projectDiv.find('.branch-status').html(
                    $('<button/>')
                        .addClass('btn btn-sm btn-danger px-3')
                        .prop('disabled', true)
                        .html('FAILED')
                );
            });
    }

    /////////////////////////////////////////////////////////////

    App.prototype.init = async function () {
        await this.getGroupDetails();

        for (const project of this.projects)
            await this.getBranchesOfProject(project);

        for (const project of this.projects)
            if (typeof project.branches !== 'undefined' && project.branches.length > 0)
                await this.getCheckPackagesOfProject(project);
    }

    /////////////////////////////////////////////////////////////

    $(function () {
        (new App).init();
    });

    //
})(window.GitLabPCRM, window.jQuery));
