<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Private Composer Package Management</title>

    <link rel="stylesheet" href="{{ route('laravel_gitlab_pcrm.static.css') }}?v=2022-06-10">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css"
        integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
</head>

<body class="bg-secondary py-4">
    <div class="container">
        <div class="card shadow mb-4">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-auto mb-2 mb-sm-0">
                        <div class="ratio ratio-1x1 bg-primary rounded text-white fs-2" style="width: 80px">
                            <div id="group-avatar" class="d-flex justify-content-center align-items-center">
                            </div>
                        </div>
                    </div>
                    <div class="col pt-2">
                        <h2 id="group-name" class="h5 text-truncate">
                            ...
                        </h2>
                        <small class="text-muted">
                            Group ID: <span id="group-id">...</span>
                        </small>
                    </div>
                </div>
            </div>
        </div>

        <div class="card shadow">
            <div id="packages" class="card-body">
                <div class="d-flex justify-content-center p-4">
                    <div class="spinner-border text-primary" style="width: 3rem; height: 3rem;" role="status">
                        <span class="visually-hidden">Loading...</span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        window.GitLabPCRM = {
            "path": "{{ config('services.laravel_gitlab_pcrm.path', 'gitlab-pcrm') }}",
            "group": @json($group)

        };
    </script>
    <script src="https://code.jquery.com/jquery-3.6.0.slim.min.js"
        integrity="sha256-u7e5khyithlIdTpu22PHhENmPcRdFiHRjhAuHcs05RI=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/1.0.0-alpha.1/axios.min.js"
        integrity="sha512-xIPqqrfvUAc/Cspuj7Bq0UtHNo/5qkdyngx6Vwt+tmbvTLDszzXM0G6c91LXmGrRx8KEPulT+AfOOez+TeVylg=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdn.jsdelivr.net/npm/moment@2.29.3/moment.js"
        integrity="sha256-QFUAPBVOV/hHtZxyDylXJ6v4jNIb121Mb3qbGpp/4oQ=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/11.4.17/sweetalert2.all.min.js"
        integrity="sha512-YXld82fwBSmWdqBekCK0oFyquMBiHZGGQnvGjFMwwx4+uxTdHP7DSisFWoFQiZ+G/43mnAmrY/fMdXuDVDdAKQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="{{ route('laravel_gitlab_pcrm.static.javascript') }}?v=2022-06-10"></script>

</body>

</html>