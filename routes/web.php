<?php

namespace HasanQQ\LaravelGitLabPCRM\Http\Controllers;

use Illuminate\Support\Facades\Route;

// get static assets from controller
Route::get('/static/application.js', [AssetController::class, 'javascript'])->name('static.javascript');
Route::get('/static/application.css', [AssetController::class, 'css'])->name('static.css');

Route::get('/{namespace}', GitLab\GroupDetailsController::class)->name('group.view');
Route::get('/{namespace}/details', [GitLab\GroupDetailsController::class, 'details'])->name('group.details');

Route::get('/projects/{id}/branches', [GitLab\ProjectDetailsController::class, 'branches'])->name('project.branches');
Route::post('/projects/{id}/publish', [GitLab\ProjectDetailsController::class, 'publish'])->name('project.publish');

Route::get('/projects/{namespace}/{repository}/packages', [GitLab\ProjectDetailsController::class, 'packages'])->name('project.packages');
